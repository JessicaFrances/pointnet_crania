from __future__ import print_function

import sys
sys.path.insert(0, '../pointnet.pytorch')

import argparse
import os
import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
from datasets import PartDataset
from pointnet import PointNetCls
import torch.nn.functional as F



parser = argparse.ArgumentParser()
parser.add_argument('--batchSize', type=int, default=4, help='input batch size')
parser.add_argument('--num_points', type=int, default=2500, help='input batch size')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=4)
parser.add_argument('--nepoch', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--outf', type=str, default='cls',  help='output folder')
parser.add_argument('--model', type=str, default = '',  help='model path')

opt = parser.parse_args()
print(opt)

# Setting the RNG seed.
#~ opt.manualSeed = random.randint(1, 10000) # fix seed
opt.manualSeed = 42 # seed initiliazes the random number generator (pseudorandom). Initialises tensors (initial state) of the model differently.
print("Random Seed: ", opt.manualSeed)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)

try:
    os.makedirs(opt.outf)
except OSError:
    pass








# Additional packages
import torch
import pandas
import time
import numpy as np

# Variables.
dataset_filename = 'dataset.csv'
output_filename = 'dataset_processed.csv'
classification = 'sex'
#~ classification = 'ancestry'
#~ classification = 'sex_ancestry'
holdout_fraction = .2		# Fraction of the dataset which is used for evaluation purposes.

# Reading the dataset.
dataset = pandas.read_csv(dataset_filename)


# Classes.
ancestry_classes = np.unique(dataset['collection'].values).tolist()
sex_classes = np.unique(dataset['sex'].values).tolist()
sex_ancestry_classes = [ sex + ' ' + ancestry for sex in sex_classes for ancestry in ancestry_classes ]

# Specifying which classes are used for the classification task.
if ( classification == 'sex' ):
	classes = sex_classes
elif ( classification == 'ancestry' ):
	classes = ancestry_classes
else:
	classes = sex_ancestry_classes

num_classes = len(classes)
print('The following classes will be used for classification: {classes}.'.format(classes = classes))

# Creating the NN.
classifier = PointNetCls(k = num_classes)

if opt.model != '':
    classifier.load_state_dict(torch.load(opt.model))

optimizer = optim.SGD(classifier.parameters(), lr=0.01, momentum=0.9) #specifies the optimization method used and its parameters, epoch step size = lr x (momentum)^epoch, momentum lower than 1
classifier.cuda()

# Displaying the proportions of the dataset belonging to each class.

# Splitting the dataset into a training and an evaluation dataset.
#	https://stackoverflow.com/a/35531218
training_dataset = dataset.sample(frac=1-holdout_fraction, random_state=200)
evaluation_dataset = dataset.drop(training_dataset.index)

print( len(training_dataset) )
print( len(evaluation_dataset) )

# Determining the number of samples, batches and samples omitted.
#~ num_samples = len(dataset)
num_samples = len(training_dataset)
num_batches = num_samples // opt.batchSize
num_samples_processed = num_batches * opt.batchSize
num_samples_omitted = num_samples - num_samples_processed

print('The dataset has {num_samples} samples available.'.format(num_samples = num_samples))
print('The dataset will be split in {num_batches} batches of data.'.format(num_batches = num_batches))
print('As a result, {num_samples_omitted} sample(s) will not be processed.'.format(num_samples_omitted = num_samples_omitted))

# Truncating the dataset to remove any omitted sample.
# As a result, the predicted classes can be properly inserted within
# the dataframe.
#~ training_dataset = training_dataset.sort_values(by='collection')
#~ training_dataset = training_dataset.sort_index
#~ training_dataset = training_dataset.truncate(after = num_samples_processed-1)
training_dataset = training_dataset[:num_samples_processed-1]

print('Length of the training dataset: {size}.'.format(size = num_samples_processed))

# For each epoch,
for epoch in range(opt.nepoch):
	# Initialising the accuracy and the computed class values.
	accuracy = 0.
	computed_classes = []
	
	for i in range(num_batches):
		# Initialising the data arrays (the vertices & the target classes).
		vertices = []
		target = []
		
		# Browsing the dataset
		for index, sample in training_dataset[i*opt.batchSize:(i+1)*opt.batchSize].iterrows():
			#~ print(index, sample)
			
			start = time.time()
			#~ print('Reading {file}.'.format(file = sample.path))
			
			# Reading the vertices from the mesh file.
			# Doing this manually because three existing packages that deal with
			# .obj files, namely `pywavefront`, `pymesh` and `cgkit` either: were
			# slow, returned an error, or simply couldn't be installed.
			pc = []
			with open(sample.path) as f:
				for line in f:
					if ( line.startswith('v') ):
						# Finding the positions of each spaces on the line.
						pos1 = line.find(' ', 2)
						pos2 = line.find(' ', pos1+1)
						pos3 = line.find(' ', pos2+1)
						
						# Defining the new vertex.
						v = [ float(line[2:pos1]), float(line[pos1+1:pos2]), float(line[pos2+1:-1]) ]
						
						# Adding the new vertex to the array of vertices.
						pc.append(v)
			
			#~ print('Reading the mesh took {duration:.3f} seconds'.format(duration = time.time()-start))
			
			# Appending this sample's point cloud to the vertex array.
			vertices.append(pc)
			
			# Appending this sample's collection to the target array.
			if ( classification == 'sex' ):
				sample_target = sample.sex
			elif ( classification == 'ancestry' ):
				sample_target = sample.collection
			else:
				sample_target = sample.sex + ' ' + sample.collection
			
			target.append([classes.index(sample_target)])
		
		# Converting the lists to pytorch tensors.
		vertices, target = torch.as_tensor(vertices), torch.as_tensor(target)
		
		# Training the model on the training dataset.
		vertices, target = Variable(vertices), Variable(target[:,0])
		vertices = vertices.transpose(2,1)
		vertices, target = vertices.cuda(), target.cuda()
		optimizer.zero_grad()
		classifier = classifier.train()
		pred, _ = classifier(vertices)
		loss = F.nll_loss(pred, target)
		loss.backward()
		optimizer.step()
		
		# Getting the predicted classes, computing batch and dataset accuracy.
		pred_choice = pred.data.max(1)[1]
		#~ print('Predicted classes', pred.data.max(1)[1])
		#~ print('Actual classes', target.data)
		#~ exit()
		correct = pred_choice.eq(target.data).cpu().sum()
		batch_accuracy = correct.item() / float(opt.batchSize)
		accuracy += correct.item() / float(opt.batchSize)
		computed_classes += pred_choice.tolist()
		#~ print('[%d: %d/%d] train loss: %f accuracy: %f' %(epoch, i+1, num_batches, loss.item(), batch_accuracy))
	
	# Printing the accuracy.
	accuracy /= num_batches
	print('Accuracy: {accuracy:.3f}.'.format(accuracy = accuracy))
	
	# Saving the computed classes into the dataset.
	#~ print('Results', computed_classes)
	#~ dataset['computed_' + classification] = computed_classes
	training_dataset['computed_' + classification] = [ classes[c] for c in computed_classes ]
	
	# Saving the dataset.
	training_dataset.to_csv(output_filename, index = False)
	
	# Saving the model's parameters.
	#~ torch.save(classifier.state_dict(), '%s/cls_model_%d.pth' % (opt.outf, epoch))
	if ( accuracy > .85 ):
		torch.save(classifier.state_dict(), '%s/sex_85_it%s.pth' % (opt.outf, epoch))
		#~ break

