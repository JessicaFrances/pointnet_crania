import os
import pandas
import xlrd	# required by pandas for excel support
#~ from collections import namedtuple



### Variables.

excel_path = '../Dropbox/PhD/MACHINE LEARNING CATEGORIES/'	# path to the excel files that lists each sample

#~ mesh_path = '/media/jessica/PhD_1/subsampled_meshes/'	# path to the mesh files
mesh_path = '/media/jessica/PhD_2/subsampled_meshes_2500/'	# path to the mesh files
file_patterns = [
	#~ '{id}_Cranium_subsampled_1000.obj'
	#~ '{id}_Cranium_subsampled_2500.obj'
	#~ '{id}_Cranium_subsampled_10000.obj'
	'{id}_Cranium.obj'
]

dataset_filename = 'dataset.csv'



### Initialising the dataset.

dataset = pandas.DataFrame()



### Reading all files in `excel_path`.
for filename in os.listdir(excel_path):
	# Extracting the collection name from the filename.
	collection = filename[:filename.index(' ')]
	sample_key = 'Sample # ({collection})'.format(collection = collection)
	
	# Reading the excel file.
	file_path = excel_path +  filename
	print('Reading {path}.'.format(path = file_path))
	df = pandas.read_excel(file_path)
	print('{num_samples} samples have been found.'.format(num_samples = df.shape[0]))
	
	# Only keeping the sample number, age and sex columns.
	if ( sample_key in df.columns ):
		df = df[[sample_key, 'Age', 'Sex']]
	else:
		df = df[['Context', 'Age', 'Sex']]
	
	# Renaming the columns.
	df.columns = ['number', 'age', 'sex']
	
	# Optional: sorting by increasing sample number.
	df = df.sort_values(by='number')
	
	# Adding a collection column.
	#	https://stackoverflow.com/a/13316001
	df.insert(0, 'collection', collection)
	#~ df['collection'] = collection
	
	# Adding this dataframe to the current dataset.
	dataset = pandas.concat([dataset, df])

print('The complete dataset is composed of {num_samples} samples.'.format(num_samples = dataset.shape[0]))
print(dataset)



### Checking which samples have an available mesh file.

def find_mesh_file(_sample):
	# Determining the sample's ID, e.g. NU021.
	sample_id = '{collection}{number:03d}'.format(collection = _sample.collection, number = _sample.number)
	
	# For each `file_patterns`,
	for file_pattern in file_patterns:
		# Creating the mesh file path associated to the current `file_pattern`
		# for the current sample.
		mesh_file = ('{path}' + file_pattern).format(
			path = mesh_path,
			collection = _sample.collection,
			id = sample_id
		)
		
		# If the mesh file exists, then that path is returned.
		if ( os.path.isfile(mesh_file) ):
			#~ print('{id}\tFOUND\t{file}'.format(id = sample_id, file = mesh_file))
			return mesh_file
	
	# If none of the file patterns yielded no correct path, then the
	# empty string is returned.
	#~ print('{id}\tNOT FOUND\t{mesh_file}'.format(id = sample_id, mesh_file = mesh_file))
	return "";

print('Filtering samples which do not have an associated mesh file.')
dataset['path'] = dataset.apply(find_mesh_file, axis=1)



### Filtering samples that do not have an associated mesh file.
dataset = dataset[dataset.path != ""]

if ( dataset.empty ):
	print('The usable dataset has no sample.')
else:
	print('The usable dataset has {num_samples} samples.'.format(num_samples = dataset.shape[0]))



### Saving the dataset to a .csv
print('Saving the usable dataset to {path}.'.format(path = dataset_filename))
dataset.to_csv(dataset_filename, index = False)
