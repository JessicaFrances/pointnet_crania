# Installling python3 & pip3.

sudo apt-get update
sudo apt-get -y install python3 python3-pip


# Upgrading pip.

sudo -H pip3 install --upgrade pip



# Installing PyTorch.
#	https://pytorch.org/get-started/locally/

### It may be required to also install cuda80 here

#~ pip3 install torch torchvision

sudo -H pip3 install pandas xlrd pymesh
